
@extends('layouts.app')
@section('leftbar')
@endsection
@section('content')

        @if(App\Post::first())
            <div class="row">
            @foreach(App\Post::where('confirmed','!=','null')->orderBy('created_at','desc')->take(20)->get() as $post)
                <div class="card col-md-3" style="width: 18rem;">
                    <img class="card-img-top" src="{{ asset('uploads/thumb/'.$post->images)}}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">{{$post->name}}</h5>
                        <p class="card-text">{{$post->Post}}</p>
                        <a href="{{route('home.show',$post->id)}}" class="btn btn-primary">Comments</a>

                    </div>
                </div>
            @endforeach
            </div>
        @endif
@endsection