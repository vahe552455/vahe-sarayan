<div>
    <h2>PostName: {{ $postName }}</h2>
    Post: {{$post}}
    <br/>
    <img src="{{asset('uploads/thumb/'.$image)}}" alt="">
    @component('mail::button', ['url' => ''])
        Button
    @endcomponent
    @component('mail::button', ['url' => '', 'color' => 'success'])
        Button
    @endcomponent
    @component('mail::button', ['url' => 'http://laravelf.com/admin/posts/'.$id.'/edit', 'color' => 'success'])
        Button
    @endcomponent

    <form method ='GET' action="{{route('posts.edit',['id'=>$id])}}"class="d-inline-block" >
        @csrf
        <button class="btn btn-primary" type ='submit'> Edit</button> </form>
</div>
