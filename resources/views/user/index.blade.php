@extends('layouts.app')
@section('content')

    @if(isset($posts[0]->name))

        <div class="row">
            @foreach($posts as $post)
                <div class="card col-md-3" style="width: 18rem;">
                    <img class="card-img-top" src="{{ asset('uploads/thumb/'.$post->images)}}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">{{$post->name}}</h5>
                        <p class="card-text">{{$post->Post}}</p>
                        <a href="{{route('home.show',$post->id)}}" class="btn btn-primary">See post</a>
                    </div>
                </div>
            @endforeach
        </div>

    @else
        <h2>No result</h2>
    @endif
@endsection