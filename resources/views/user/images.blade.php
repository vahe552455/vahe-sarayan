
@extends('layouts.app')
@section('content')
    @if($errors->any())
        <p class="alert">{{$errors->all()}}</p>
    @endif
    <form method='POST' action="{{url('images')}}" enctype="multipart/form-data" class="">
        @csrf

        <div class="form-group row mt-2">
            <label for="name" class="col-md-4 col-form-label text-md-right"> Upload</label>
            <div class="col-md-8 ">
                <input type="file" name="image" class="form-control col-md-8 d-inline-block">
                <button type="submit" >Submit</button>
            </div>
        </div>
    </form>


    <div class="row mt-4 bg-dark ">
        @foreach(\App\image::where('user_id','!=','null')->get() as $key => $image)
            <div class="col-4 ">
                <div class="img-open">
                    <img src="{{asset('uploads/thumb/'.$image->name)}}" alt="" class="img-fluid h-100 " data-id="{{$key}}" id="{{$image->id}}">
                </div>
            </div>
        @endforeach
    </div>
    <div class="our-modal  d-none">
        <div class="w-100 h-100 d-flex justify-content-center align-items-center">
            <div class="w-75  bg-light position-relative appends">
                <div class="">
                    @foreach(\App\image::where('user_id','!=','null')->get() as $key =>$image)
                        <div data-id="{{$key}}"><img src="{{asset('uploads/'.$image->name)}}" alt="" class="img-fluid w-100 h-100"> </div>
                    @endforeach
                </div>
                <div class="d-flex align-items-center justify-content-around  w-100 h-100 button-side">
                    <button class="left mr-5"><i class="fas fa-chevron-left"></i></button>
                    <button class="right ml-5"><i class="fas fa-chevron-right"></i></button>
                </div>
            </div>
            <div class="w-25 h-75 bg-light clearfix">
                <div class="dropdown d-inline-block">
                    <button class="btn dropdown-toggle bg-light" type="button" data-toggle="dropdown">option
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="#">change this</a></li>
                        <li><a href="{{route('cut',isset($data)? $data:'')
                        }}">cut</a></li>
                        <li><a href="#">delete</a></li>
                    </ul>
                </div>
                <div class="x-it d-inline-block float-right mr-4" data-x="our-modal"><i class="fas fa-times"></i></div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $(document).on('click','.img-open',open)
            function open(e) {
                ajax(e);
                var id = e.target.getAttribute('data-id');
                $('.our-modal').fadeIn();
                $('.our-modal').addClass('d-flex');
//                get id clicked element
                $('.appends').find("[data-id='"+id+"']");
//                fadout all fadein clicked
                $('.appends div:first-child div').fadeOut('fast');
                $('.appends').find("[data-id='"+id+"']").fadeIn('fast');
               $('.button-side button').off('click').on('click',(e)=>{
                    $('.appends').find("[data-id='"+id+"']").fadeOut('fast');
                if($(e.currentTarget).hasClass('left')) {
                    --id;
                    if(id == -1) {
                        id = $('div[data-id]').length-1;
                    }
                }
                else {
                console.log(1);
                    ++id;
                    if(id== $('div[data-id]').length) {
                        id = 0;
                    }
                };
                $('.appends').find("[data-id='"+id+"']").fadeIn('fast');
            });
                $(document).on('click','.x-it',(e)=>{
                    var close = e.currentTarget.getAttribute('data-x');
                $('.'+close).fadeOut('fast');
                $('.'+close).removeClass('d-flex');
            })
            };
function ajax(e) {
        $.ajax({
            url:'openimage',
            type:'GET',
            data:{
                id:e.target.id,
                _token:'{{csrf_token()}}'
            },
            success: function (data) {
               console.log(data);
            }
        })
}
        })
    </script>
@endsection