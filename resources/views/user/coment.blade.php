
@extends('layouts.app')

@section('content')
    <div class="container">

        <img src="{{asset('uploads/'.$post->images)}}" alt="" class="img-fluid">
        <h2 class="text-center">Coments for {{$post->name}} </h2>

        <div class="card messages">
            <div class="card-body">
                @foreach($post->comments()->orderBy('created_at','desc')->take(50)->get()->reverse() as $coment)
                    <div class="card card-inner">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                    <p class="text-secondary text-center">15 Minutes Ago</p>
                                </div>
                                <div class="col-md-10">
                                    <p><a href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>{{$coment->user->name}}</strong></a></p>
                                    <p>{{$coment->text}}</p>
                                    <p>
                                        <a class="float-right btn btn-outline-primary ml-2">  <i class="fa fa-reply"></i> Reply</a>
                                        <a class="float-right btn text-white btn-danger"> <i class="fa fa-heart"></i> Like</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach


            </div>

        </div>
        <form method="POST" action="{{route('home.store',$post->id)}}" class="form-controll type_msg" >
            @csrf
            <div class="input-group mt-3 ">
                <input type="text" class="form-control py-2" placeholder="Write message" aria-label="Recipient's username" aria-describedby="button-addon2" name="comment">
                <button class="btn btn-primary btn-sm" id="btn-chat"><i class="far fa-comments" name="send" ></i></button>
            </div>
        </form>

    </div>

    <script>
        @if(auth()->user())
    $(document).ready(function(){
            $('.type_msg').on('submit',function(e) {
                e.preventDefault();
                var text = $(e.target).find('input.form-control');

                $.ajax({
                    type:'POST',
                    url:'{{route('home.store')}}',
                    data: {
                        text : text.val(),
                        id : '{{$post->id}}',
                        _token :'{{csrf_token()}}'
                    },
                    success: function(data) {
                        console.log(data[0].text);
                        var html =$('.messages').html();
                        var message = `<div class="card card-inner">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                        <p class="text-secondary text-center">15 Minutes Ago</p>
                                    </div>
                                    <div class="col-md-10">
                                        <p><a href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>`+'@if(auth()->user()){{(auth()->user()->name)}}@endif'+`</strong></a></p>
                                        <p>`+data[0].text+`</p>
                                        <p>
                                            <a class="float-right btn btn-outline-primary ml-2">  <i class="fa fa-reply"></i> Reply</a>
                                            <a class="float-right btn text-white btn-danger"> <i class="fa fa-heart"></i> Like</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>`;
                        $('.messages').html(html+message);
                        $(text).val('');
                    }
                })
            })
        })
        @else
        @endif
    </script>

@endsection
