@extends('layouts.app')
@section('content')

    <div class="container">
        <h3 class=" text-center">Messaging</h3>
        <div class="messaging">
            <div class="inbox_msg">
                <div class="inbox_people">
                    <div class="headind_srch">
                        <div class="recent_heading">
                            <h4>Recent</h4>
                        </div>
                        <div class="srch_bar">
                            <div class="stylish-input-group">
                                <input type="text" class="search-bar"  placeholder="Search" >
                                <span class="input-group-addon">
                <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                </span> </div>
                        </div>
                    </div>
                    <div class="inbox_chat">
                        {{--Js kod in public/js/my.js --}}
                        @foreach(App\User::where('id','!=',auth()->user()->id)->get() as $user )
                            <div class="chat_list " id="{{$user->id}}" >
                                <div class="chat_people">
                                    <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                                    <div class="chat_ib">
                                        <h5>{{$user->name}}<span class="chat_date">Dec 25</span></h5>
                                        @if(!empty($user->chats()->where('to_id',auth()->user()->id)->get()->first()))

                                            <p>{{ $user->chats()->where('to_id',auth()->user()->id)->orderBy('created_at','desc')->get()->first()->msg}}</p>

                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="mesgs">
                    <div class="msg_history">

                        <div class="incoming_msg">
                            <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                            <div class="received_msg">
                                <div class="received_withd_msg">
                                    <p>Click to friend to talk with her</p>
                                    <span class="time_date"> 11:01 AM    |    Today</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="type_msg">
                        <div class="input_msg_write">
                            <input type="text" class="write_msg" placeholder="Type a message" />
                            <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>


            {{--   <p class="text-center top_spac"> Design by <a target="_blank" href="#">Sunil Rajput</a></p>--}}
        </div>
    </div>

    <script>
        $('.chat_list').on('click', function(e) {
//            console.log(e.currentTarget);
            var to_id = e.currentTarget.id;
            var messages ='messages';
//            console.log(id);
            $('.active_chat').removeClass('active_chat');
            $(this).addClass('active_chat');
            $.ajax({
                url:'/messages',
                type: 'POST',
                data : {
                    id:to_id,
                    _token:'{{csrf_token()}} '
                },
                success: function(data) {
//                    console.log(data[0]);
                    $('.msg_history').html('');
                    var mesages =$('.msg_history');
                    $.each(data[0], function(index , item) {
                        if(item.user_id =='{{auth()->user()->id}}'){
                            var outgoing =`<div class="outgoing_msg">
                                    <div class="sent_msg">
                                    <p>`+item.msg+`</p>
                                <span class="time_date"> 11:01 AM    |    June 9</span> </div>
                                </div>`;
//                                console.log(outgoing);
                            $('.msg_history').append(outgoing);
                        }
                        else {
                            var incoming = `<div class="incoming_msg">
                             <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                             <div class="received_msg">
                             <div class="received_withd_msg">
                             <p>`+item.msg+`</p>
                             <span class="time_date"> 11:01 AM    |    Yesterday</span></div>
                             </div>
                             </div>`;
                            $('.msg_history').append(incoming);
                        }
                    })
                }
            })
            $(document).on('click' ,'.msg_send_btn',function(e) {
                console.log($(this).parents('.type_msg').find('input').val())

                var message = $(this).parents('.type_msg').find('input');
                $.ajax({
                    url:'messages/get',
                    type:'POST',
                    data:{
                        _token: '{{csrf_token()}}',
                        message: message.val(),
                        id: to_id
                    },
                    success: function (e) {
                        var outgoing =`<div class="outgoing_msg">
                               <div class="sent_msg">
                                 <p>`+e[0].msg+`</p>
                                <span class="time_date"> 11:01 AM    |    June 9</span> </div></div>`;
                        var height = $('.msg_history').prop('scrollHeight')
                        console.log(height)
                        $('.msg_history').append(outgoing);

                        $('.msg_send_btn').attr('disabled',false);

                        $('.msg_history').animate({scrollTop:height+'px'},1000)
                    }

            })
                $('.msg_send_btn').attr('disabled',true);
                message.val('');

            })
        })</script>

@endsection
{{--   ctrl+alt+i   clean code--}}
