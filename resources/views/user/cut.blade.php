@extends('layouts.app')
@section('content')
<div class="our-modal  d-none">
    <div class="w-100 h-100 d-flex justify-content-center align-items-center">
        <div class="w-75  bg-light position-relative appends">
            <div class="">
                @foreach(\App\image::where('user_id','!=','null')->get() as $key =>$image)
                    <div data-id="{{$key}}"> <img src="{{asset('uploads/'.$image->name)}}" alt="" class="img-fluid w-100 h-100"></div>
                @endforeach
            </div>
            <div class="d-flex align-items-center justify-content-around  w-100 h-100 button-side">
                <button class="left mr-5"><i class="fas fa-chevron-left"></i></button>
                <button class="right ml-5"><i class="fas fa-chevron-right"></i></button>
            </div>
        </div>
        <div class="w-25 h-75 bg-light clearfix">
            <div class="dropdown d-inline-block">
                <button class="btn dropdown-toggle bg-light" type="button" data-toggle="dropdown">option
                    <span class="caret"></span></button>
                {{--<ul class="dropdown-menu">--}}
                    {{--<li><a href="#">change this</a></li>--}}
                    {{--<li><a href="">cut</a></li>--}}
                    {{--<li><a href="#">delete</a></li>--}}
                {{--</ul>--}}
            </div>
            <div class="x-it d-inline-block float-right mr-4" data-x="our-modal"><i class="fas fa-times"></i></div>
        </div>
    </div>
</div>
@endsection