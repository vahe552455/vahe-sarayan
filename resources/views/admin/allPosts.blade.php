@extends('layouts.app')
@section ('content')

    <div class="container">
        <h3 class="text-center">Posts</h3>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <table class="table mt-3 jquery_table"  >
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Post</th>
                        <th>Action</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>

                        @foreach(App\Post::all() as $item)
                            <tr>
                                <td>{{ $item->name}} </td>
                                <td>{{ $item->Post }}</td>
                                <td>

                                    <form method ='GET' action="{{route('posts.edit',['id'=>$item->id])}}"class="d-inline-block" >
                                        @csrf
                                        <button class="btn btn-primary" type ='submit'> Edit</button> </form></td>
                                <td>
                                    <form method ='POST'
                                          action="{{route('posts.destroy',['id'=>$item->id])}}" class="d-inline-block ml-2" onclick="return confirm('Are you sure you want to delete this item?')">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-primary ml-2" type ='submit'>Delete </button>
                                    </form>
                                </td>
                                {{--<td>
                                    <form method ='POST' action="{{route('posts.move',['post'=>$item->id])}}" class="d-inline-block" >
                                        @csrf
                                        <button class="btn btn-primary ml-2" type ='submit'onclick="return confirm('Back for change something?')">Send </button>

                                    </form>
                                </td>--}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endsection