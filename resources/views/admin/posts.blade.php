@extends('layouts.app')
@section ('content')

    <div class="col-md-8">
        {{--{{Breadcrumbs::render('products',$data->name,'Products')}}--}}

        <div class="card">
            <div class="card-header">Add Post</div> <div class="card-body">
                <form method="POST" action="{{route('posts.store')}} " enctype="multipart/form-data">

                    @csrf
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label text-md-right"> Name</label>
                        <div class="col-md-6"><input id="name" type="text" name="name" value="" required="required" autofocus="autofocus" class="form-control"></div>
                    </div>
                    <div class="form-group row">
                        <label for="text" class="col-md-4 col-form-label text-md-right">Post</label>
                        <div class="col-md-6"><textarea id="password"  name="posts" required="required" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="row ">
                        <label for="name" class="col-md-4 col-form-label text-md-right"> Category</label>
                        <div class="col-md-7 text-center ">
                          {{--  <select name="category[]" multiple   class="form-control ">
                                @foreach(App\Category::all() as $item)
                                    <option  value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                            </select>--}}
                            <select class="js-example-basic-multiple w-75" name="category[]" multiple="multiple">
                                @foreach(App\Category::all() as $item)
                                    <option  value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row mt-2">
                        <label for="name" class="col-md-4 col-form-label text-md-right"> Upload</label>
                        <div class="col-md-8 ">

                            <input type="file" name="image" class="form-control col-md-8"/>
                        </div>
                    </div>
                     <div class="form-group row mt-2"><div class="col-md-8 offset-md-4"><button type="submit" class="btn btn-primary">
                                Change
                            </button>
                        </div>
                    </div>

                </form>
                @if(isset($errors))
                    @foreach($errors->all() as $error)
                    <div>{{$error}}</div>
                    @endforeach
                @endif
                @if(isset($message))<div>{{$message}}</div>@endif
            </div>
        </div>
    </div>

@endsection