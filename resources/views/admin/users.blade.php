@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <table class="table jquery_table dataTable mt-3" id="table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($users as $item)
                        <tr>
                            <td>{{ $item->name}} </td>
                            <td>{{ $item->email }}</td>
                            <td>
                                <form method ='GET' action="{{route('users.edit',['user'=>$item->id])}}" class="d-inline-block">
                                    @csrf
                                    <button class="btn" type ='submit'> Edit</button> </form>
                                <form method ='POST'
                                                                                            action="{{route('users.destroy',['id'=>$item->id])}}" class="d-inline-block" >
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-primary ml-2" type ='submit'onclick="return confirm('Are you sure you want to delete this item?')">Delete </button>
                                    </form>
                                </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
     /*   $(document).ready(function () {
            $('#table').DataTable();
        } );*/
        /*$(document).ready(function () {
         $(document).on('click','.role button',function (){
         $id =$(this).data('id');
         $role =$(this).text();
         var thistag =$(this);
         alert($id );
         $.ajax({
         type: 'GET',
         url: '/change',
         data: {id :$id,role:$role},
         success: function (data) {
         console.log($("[data-get=id]"));
         var id = data[0].id;
         $("[data-get='"+id+"']").text(data[0].role);
         }
         })
         })

         })*/
    </script>

@endsection

