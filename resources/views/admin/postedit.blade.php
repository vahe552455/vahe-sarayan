@extends('layouts.app')
@section ('content')

    <div class="col-md-8">
        {{--{{Breadcrumbs::render('products',$data->name,'Products')}}--}}

        <div class="card container-fluid">
            <div class="card-header row">Change Post</div> <div class="card-body">
                <form method="POST" action="{{route('posts.update',$data->id)}}  ">
                    @method('PUT')
                    @csrf
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label text-md-right"> Name</label>
                        <div class="col-md-6"><input id="name" type="text" name="name" value="{{$data->name}}" required="required" autofocus="autofocus" class="form-control"></div>
                    </div>
                    <div class="form-group row">
                        <label for="text" class="col-md-4 col-form-label text-md-right">Post</label>
                        <div class="col-md-6"><textarea id="password"  name="posts" required="required" class="form-control">{{$data->Post}}</textarea>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-8 text-center ">

                        </div>
                    </div>


                    <div class="row justify-content-center">
                        <div class="col-md-8 text-center ">
                            <select name="category" class="form-control ">
                                @foreach(App\Category::all() as $item)
                                <option  value="user">{{$item->name}}</option>
                       @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mt-2"><div class="col-md-8 offset-md-4"><button type="submit" class="btn btn-primary">
                                Change
                            </button>

                        </div>
                    </div>
                </form>
              {{--  @role('admin')
                <form method ='POST' action="{{route('posts.move',['post'=>$data->id])}}" class="text-right" >
                    @csrf
                    <button class="btn btn-primary ml-2 " type ='submit'onclick="return confirm('Back for change something?')">Send </button>
                </form>
                @endrole--}}
            </div>
        </div>
    </div>

@endsection