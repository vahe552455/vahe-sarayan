@extends('layouts.app')
@section ('content')

    <div class="col-md-8">
        {{--{{Breadcrumbs::render('products',$data->name,'Products')}}--}}

        <div class="card">
            <div class="card-header">Change Category</div> <div class="card-body">
                <form method="POST" action="{{route('category.update',$data->id)}}">
                    @method('PUT')
                    @csrf
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label text-md-right"> Name</label>
                        <div class="col-md-6"><input id="name" type="text" name="name" value="{{$data->name}}" required="required" autofocus="autofocus" class="form-control"></div>
                    </div>
                    <div class="form-group row">
                        <label for="text" class="col-md-4 col-form-label text-md-right">Description</label>
                        <div class="col-md-6"><textarea id="password"  name="description" class="form-control">{{$data->description}}</textarea>
                        </div>
                    </div>

                    <div class="form-group row mt-2"><div class="col-md-8 offset-md-4"><button type="submit" class="btn btn-primary">
                                Change
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection