@extends('layouts.app')
@section ('content')
    <div class="col-md-8">
        {{--{{Breadcrumbs::render('products',$data->name,'Products')}}--}}

        <div class="card">
            <div class="card-header">Add Category</div>
            <div class="card-body">
                <form method="POST" action="{{route('category.store',['id'=>auth()->user()->id])}}">

                    @csrf
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label text-md-right"> Name</label>
                        <div class="col-md-6"><input id="name" type="text" name="name" value="" required="required" autofocus="autofocus" class="form-control"></div>
                    </div>
                    <div class="form-group row">
                        <label for="text" class="col-md-4 col-form-label text-md-right">Category</label>
                        <div class="col-md-6"><textarea id="password"  name="description" required="required" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group row mt-2"><div class="col-md-8 offset-md-4"><button type="submit" class="btn btn-primary">
                                Add
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection