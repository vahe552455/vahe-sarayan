@extends('layouts.app')
@section ('content')
    <div><a href="{{route('category.create')}}" class="btn btn-primary">add</a></div>
    <div class="container">
        <h3 class="text-center">Categories</h3>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <table class="table mt-3 jquery_table"  >
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Action</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach(App\Category::all() as $item)
                        <tr>
                            <td><a href="{{route('category.posts',['id'=>$item->id])}}"> {{ $item->name}}</a> </td>
                            <td>{{ $item->description }}</td>
                            <td>

                                <form method ='GET' action="{{route('category.edit',['id'=>$item->id])}}"class="d-inline-block" >
                                    @csrf
                                    <button class="btn btn-primary" type ='submit'> Edit</button> </form></td>
                            <td>
                                <form method ='POST'
                                      action="{{route('category.destroy',['id'=>$item->id])}}" class="d-inline-block ml-2" onclick="return confirm('Are you sure you want to delete this item?')">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-primary ml-2" type ='submit'>Delete </button>
                                </form>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection