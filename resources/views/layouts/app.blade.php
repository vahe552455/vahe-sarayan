<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->




{{--jquery datatable--}}


<!-- Fonts -->


    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/layout.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet"  href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script>{{--@if(auth()->user())  var id = parseInt({{auth()->user()->id}})
        @endif--}}
                @if(isset($post))
        var id = parseInt({{$post->id}});
                @endif
                @if(auth()->user())
        var user_id = parseInt({{auth()->user()->id}})@endif
    </script>
    <script src="{{ asset('js/app.js') }}" ></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script type="text/javascript"  src="{{asset('js/my.js')}}"></script>
</head>
<body>
<div id="app">
    @section('header')
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark py-4">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarColor01">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{url('')}}">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('messages')}}">Messages</a>
                    </li>
                    @role('admin')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('posts.index')}}">Notification
                            <i class="far fa-bell">@if(App\Post::where('confirmed','0')) {{count(App\Post::where([
                        ['confirmed',0],
                        ['moved',0]
                    ])->get() )}} @else 0 @endif</i>                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('posts.all')}}">Posts</a>
                    </li>
                    @endrole
                    @role('moderator')
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('posts.index')
                        }}">Notifications
                            <i class="far fa-bell">
                                @if(auth()->user()->Posts()->where('moved',1))
                                    {{ count(auth()->user()->Posts()->where('moved',1)->get())}}
                                @else 0
                                @endif </i> </a>
                    </li>
                    @endrole
                    @hasanyrole('moderator|admin')<li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            Categories <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            @foreach(App\Category::all() as $category)
                                <a class = "dropdown-item"  href="{{route('category.posts',['id'=>$category->id])}}">
                                    {{$category->name}}
                                </a>
                            @endforeach
                            @role('admin') <a class = "dropdown-item"  href="{{route('category.index')}}">         Categories
                            </a>
                            @endrole
                        </div>
                    </li>@else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                Categories <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @foreach(App\Category::all() as $category)
                                    <a class = "dropdown-item"  href="{{route('home.posts',['id'=>$category->id])}}">
                                        {{$category->name}}
                                    </a>
                                @endforeach
                            </div>
                        </li>

                        @endhasanyrole



                        @guest
                        <li class="nav-item">
                            @if (Route::has('register'))
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            @endif
                        </li>
                        @endguest
                </ul>
                {{--search--}}
                <form class="form-inline navbar-nav" method ='POST' action="{{route('searched.posts')}}">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="search">
                    @csrf
                    <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                </form>

                <ul class="navbar-nav">
                    @guest
                    <div class="topnav ml-2">
                        <div class="login-container">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <input type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }} py-1" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus >
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }} py-1" name="password" placeholder="Password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>
                            </form>
                        </div>
                    </div>

                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
       document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                </ul>
                @endguest
            </div>
        </nav>
    @show
    @section('leftbar')
        <div class="container-fluid">
            <div class="row">
                <aside class="col-12 col-sm-3 p-0 bg-dark">
                    <nav class="navbar navbar-expand-sm navbar-dark bg-dark align-items-start flex-sm-column flex-row">
                        <a class="navbar-brand" href="#"><i class="fa fa-bullseye fa-fw"></i> Brand</a>
                        <a href class="navbar-toggler" data-toggle="collapse" data-target=".sidebar">
                            <span class="navbar-toggler-icon"></span>
                        </a>
                        <div class="collapse navbar-collapse sidebar">
                            <ul class="flex-column navbar-nav w-100 justify-content-between">
                                {{--admine kara mtni users--}}
                                @role('admin') <li class="nav-item">
                                    <a class="nav-link pl-0  text-nowrap" href="{{route('users.index')}}">Users</a>
                                </li>
                                @endrole

                                @hasanyrole('moderator|admin')
                                <li class="nav-item">
                                    <a class="nav-link pl-0" href="{{route('posts.create')}}"><i class="fa fa-book fa-fw"></i> <span class="">Add Post</span></a>
                                </li>
                                @endrole
                                @auth
                                <li class="nav-item">
                                    <a class="nav-link pl-0" href="{{ route('images')}}"><i class="fa fa-heart fa-fw"></i> Images</a>

                                </li>
                                @endauth
                                {{--<li class="nav-item">
                                    <a class="nav-link pl-0" href="#"><i class="fa fa-heart fa-fw"></i> <span class="">Link</span></a>
                                </li>--}}

                            </ul>
                        </div>
                    </nav>
                </aside>
                @show
                <main class="py-4 col-8 m-auto">
                    @yield('content')
                </main>
            </div>
        </div>
</div>



<script type="text/javascript"  src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
{{--<script>
    var pusher = new Pusher('9066377ac8f2d91e7fd2');
    var privateChannel = pusher.subscribe('firstchanel.'+id);               privateChannel.bind('StatusLiked', function(data) {
        alert(JSON.stringify(data));
    });
    Pusher.log = function (message) {
        if (window.console && window.console.log) { window.console.log(message); }
    };
console.log(privateChannel)
</script>--}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>



</body>
</html>
