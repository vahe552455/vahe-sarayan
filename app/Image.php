<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['name','user_id','product_id'];

    public function user() {
        $this->belongsTo('App/User');
    }
}
