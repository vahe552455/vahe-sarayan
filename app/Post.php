<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable =['name','post','user_id','confirmed','moved','images'];
    public function user() {
        return $this->belongsTo('App\User');
    }
    public function categories() {
        return $this->belongsToMany('App\Category');
    }
    public function comments() {
        return $this->hasMany('App\Comment');
    }
}
