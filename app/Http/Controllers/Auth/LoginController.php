<?php
namespace App\Http\Controllers\Auth;
    use Illuminate\Support\Facades\Log;
    use Socialite;
    use App\Http\Controllers\Controller;

    use App\Traits\LoginTrait;
    class LoginController extends Controller
    {

        use LoginTrait;

        /**
         * Where to redirect users after login.
         *
         * @var string
         */
        protected $redirectTo = '/home';

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest')->except('logout');
    }
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

    }
}
