<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use App\Events\PostCreated;
use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if(auth()->user()->hasRole('admin')) {
            return view('admin.notifications');
        }
        else {
            return view('admin.backedNotifications');
        }
    }

    public function move($id )
    {
        $post = Post::findorFail($id);
        $post->update(['moved'=>1]);
        return redirect()->back();
    }

    public function all()
    {
        return view('admin.allPosts');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.posts');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'name'=>'required',
                'posts'=>'required',
                'category'=>'required',
                'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]
        );

        if($request->image) {
            $name =$request->image->getClientOriginalName();
            $fullpath=public_path().'/uploads/'.$name;

            //ete et file ka uploadsum
            if(!file_exists($fullpath)) {
                $thumbpath =public_path().'/uploads/thumb/'.$name;
                list($width, $height) = getimagesize($request->image);
                $img = Image::make($request->image);
                $img->resize($width*2/3,$height*2/3)->save($fullpath,60);
                if($width>$height) {
                    $img->resize(300,200)->save( $thumbpath,30);
                }
                else {
                    $img->resize(200,300)->save( $thumbpath,30);
                }

            }
        }
        if(auth()->user()->hasRole('admin')) {
            $post = Post::create(
                ['user_id'=>auth()->user()->id,
                    'name'=>$request->name,
                    'post'=>$request->posts,
                    'images'=>isset($name) ? $name: 'noimage.jpg',
                    'confirmed'=>1,
                ]);

        }
        else {
            $post = Post::create(
                ['user_id'=>auth()->user()->id,//auth  useri id
                    'name'=>$request->name,
                    'post'=>$request->posts,//useri post@
                    'confirmed'=>0,
                    'images'=>isset($name) ? $name: 'noimage',
                ]);
            Mail::to($request->user())->send(new OrderShipped($post));
        }
        foreach($request->category as $cat_id) {
            $category= Category::findorFail($cat_id);
            $post->categories()->attach($category);
        }

        event(new PostCreated($post));



//        ->trigger('firstChanel', 'PostCreated', [
//        'message' => 'hello world'
//    ]);
        return view('admin.posts')->with('message','created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Post::findorFail($id);
        return view('admin.postedit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   $data= Post::findorFail($id);
        $data->update($request->all());
        if(auth()->user()->hasRole('admin')) {
            $data->update(['confirmed'=>1]);
        }
        else {
            $data->update(['moved'=>0]);
        }
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Post::findorFail($id);
        $data->categories()->delete();
        $data->delete();
        return redirect()->back();
    }
}
