<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use app\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
//        $this->middleware('admin_only', ['except' => []]);
    }

    public function index()
    {   if(auth()->user()->hasRole('admin')) {
        $id = Auth::user()->id;
        $users =User::where('id','!=',$id)->get();
        return view('admin.users',compact('users'));
    }
    else {
        return view('admin.index');
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {

        $data = User::findorFail($user);
        return view('admin.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'email'=>'required',
            'name' =>'required',
            'roles' =>'required',
        ]);
        User::findorFail($id)->update([
            'name'=>$request->name,
            'email'=>$request->email,
        ]);
        User::find($id)->syncRoles($request->roles);
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=User::findorFail($id)->delete();
        $users =User ::where('id','!=',auth()->user()->id)->get();
        return view('admin.users',compact('users'));
    }
}
