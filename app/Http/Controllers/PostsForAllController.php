<?php

namespace App\Http\Controllers;

use App\Comment;
use App\User;
use Illuminate\Http\Request;
use App\Category;
use App\Post;
use App\Events\StatusLiked;

class PostsForAllController extends Controller
{
    /*public function index(Request $request) {
        if($request->role =='Admin') {
            auth()->user()->syncRoles('Moderator');
    }

        return response()->json(array($request->all()), 200);
    }*/
    public function posts($id) {

        $posts = Category::find($id)->posts()->orderBy('updated_at','desc')->take(20)->get();

        return view('user.index',compact('posts'));
    }
    public function search(Request $request) {
        $posts = Post::where('name','like','%'.$request->search.'%')->orWhere('Post','like','%'.$request->search.'%')->get();
        return view('user.index',compact('posts'));
    }
    public function show_post($id)
    {
        $post = Post::findorFail($id);
        $coment =$post->comments;
        // event(new StatusLiked($coment))->toOthers();
        return view('user.coment',compact('post') );
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'text'=>'required',
        ]);
        if(!auth()->user()) {
            return redirect()->back();
        }
        $post = Post::findorFail($request->id);
        $comment = Comment::create([
            'text'=>$request->text,
            'post_id'=>$request->id,
            'user_id'=>auth()->user()->id,
        ]);
        $comment->setAttribute('name', $comment->user->name);
        $message = broadcast(new StatusLiked($comment))->toOthers();
        return response()->json(array([ 'text'=> $request->text,'id'=>$request->id]),200);
        /*  return  redirect()->route('home.show',[$id]);*/
//            view('user.coment',compact('post'));
    }
}
