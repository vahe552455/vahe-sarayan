<?php

namespace App\Http\Controllers;

use App\Events\MessageSended;
use Illuminate\Http\Request;
use App\Chat;
class MessageController extends Controller
{
    public function index(Request $request) {
        $messages = Chat::where([
            ['user_id',auth()->user()->id],
            ['to_id',$request->id],
        ])->orWhere([
            ['user_id',$request->id],
            ['to_id',auth()->user()->id],
        ])->orderBy('created_at','asc')->get();
        return response()->json(array($messages));
    }
    public function getMessage(Request $request)
    {

        $message = Chat::create([
            'msg'=>$request->message,
            'user_id'=>auth()->user()->id,
            'to_id' =>$request->id
        ]);
        $broadcast = broadcast(new MessageSended($message))->toOthers();
        return response()->json(array($message),200);


    }
}
