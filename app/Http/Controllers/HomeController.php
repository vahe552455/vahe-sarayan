<?php

namespace App\Http\Controllers;
use App\Image;
use View;
use Image as Images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Category;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Role::create(['name'=>'admin']);
        //Permission::create(['name' =>'comment posts']);
        // Auth::user()->givePermissionTo('write posts');
        // Auth::user()->assignRole('admin');
        return view('home');
    }
    public function images(Request $request) {
        if($request->isMethod('get')) {
            return view('user/images');
        } else {
            $name = $request->image->getClientOriginalName();
            $this->validate($request,[
                'image'=>'required',
            ]);
            $fullpath=public_path().'\uploads\\'.$name;
            if(!file_exists($fullpath)) {
                $thumbpath =public_path().'/uploads/thumb/'.$name;
                list($width, $height) = getimagesize($request->image);
                $img=Images::make($request->image);
                $img->resize($width*2/3,$height*2/3)->save($fullpath,60);
                if($width>$height) {
                    $img->resize(300,200)->save( $thumbpath,30);
                }
                else {
                    $img->resize(200,300)->save( $thumbpath,30);
                }
                $req = array_merge($request->all(),
                    ['user_id' => auth()->user()->id,
                        'name'=>$name
                    ]);
                $image = Image::create($req);
                return redirect()->back()->with(['msg'=>'You already have that image ']);
            }
            return redirect()->back();
        }
    }
  /*  public function cut(Request $request) {
        if($request->isMethod('get')) {
            return view('user/cut');
        }
    }*/
    public function openimage(Request $request) {
        $image = Image::find($request->id);
        $view = View::make('user.images',['data'=>$image]);
        return $view->render();
        return response()->json(array($view));
    }
}
