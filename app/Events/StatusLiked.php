<?php

namespace App\Events;

use App\Comment;
use Illuminate\Mail\Message;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Broadcasting\PrivateChannel;

class StatusLiked implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $comment;


    /**
     * Create a new event instance.
     *
     * @return void
     */


    public function __construct($message)
    {
        $this->comment = $message;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */

    public function broadcastOn()
    {
//          return new PrivateChannel('firstchanel.1');
        return new PrivateChannel('firstchanel.'.$this->comment->post_id);

    }
}