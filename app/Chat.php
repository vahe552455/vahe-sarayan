<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable =['msg','user_id','to_id'];
    public function user() {
        $this->belongsTo('App\user');
    }
}
