<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Post;
class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $post;
    public function __construct(Post $post)
    {
        $this->post =$post;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from('example@example.com')
            ->markdown('emails.shipped') ->text('emails.shipped')->with([
                'postName' => $this->post->name,
                'post' => $this->post->post,
                'image' => $this->post->images,
                'id' => $this->post->id,
            ]);
    }
}
