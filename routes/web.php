<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

    Auth::routes(['verify' => true]);

    Route::group(['middleware'=>['isAdmin','verified'],'prefix'=>'admin','namespace'=>'Admin'],function(){
        Route::get('', 'AdminController@index')->name('admin');
        Route::get('posts/all','PostController@all')->name('posts.all');
        Route::post('posts/move/{post}
        ','PostController@move')->name('posts.move');
        Route::get('category/posts/{id}','CategoryController@posts')->name('category.posts');
       Route::resource('users', 'UserController')->middleware('admin_only');
       Route::resource('posts', 'PostController');
       Route::resource('category', 'CategoryController');
    });
//for user
    Route::group(['middleware'=>['user','verified']],function () {
        Route::get('/home', 'HomeController@index')->name('home');
        Route::match(['get','post'],'/images', 'HomeController@images')->name('images');
        Route::get('openimage','HomeController@openimage');
        Route::name('cut')->match(['get','post'],'/cut', 'HomeController@cut');
        Route::post('messages','MessageController@index');
        Route::view('/messages', 'user\message');
        Route::post('messages/get','MessageController@getMessage');

    });
    Route::get('posts/{user}', 'PostsForAllController@posts')->name('home.posts');
    Route::post('posts/search', 'PostsForAllController@search')->name('searched.posts');
    Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
    Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
    Route::get('post/{userid}','PostsForAllController@show_post')->name('home.show');
    Route::post('post/store','PostsForAllController@store')->name('home.store');

/*
Route::get('test', function () {
    event(new App\Events\StatusLiked('Someone'));
    return "Event has been sent!";
});*/


