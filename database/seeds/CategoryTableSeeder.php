<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\CategoryPost;
use App\Post;

use Illuminate\Support\Facades\DB;
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!DB::table('categories')->first()) {
            Category::create([
                'name' => 'phones',
                'description' => 'About phones'
            ]);
        }
        if (!DB::table('posts')->first()) {
            Post::create([
                'name' => 'iphone',
                'Post' => 'good choice for you',
                'user_id' =>1,
                'images' =>'noimage.jpg',
                'confirmed' =>'1'
            ]);
        } if (!DB::table('category_post')->first()) {
        $category=  CategoryPost::create([
            'category_id' =>1,
            'post_id' =>1
        ]);
        $category->timestamps= false;
    }
    }
}