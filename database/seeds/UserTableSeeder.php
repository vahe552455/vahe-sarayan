<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

use Illuminate\Support\Facades\DB;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!DB::table('users')->first()) {
            User::create([
                'name'=>'admin',
                'email'=>'a@mail.ru',
                'password'=>bcrypt('mypass'),
                'email_verified_at'=>'2018-10-26 10:12:42'
            ]);
            User::create([
                'name'=>'user',
                'email'=>'vahe.sarayan@mail.ru',
                'password'=>bcrypt('123456'),
                'email_verified_at'=>'2018-10-26 10:12:43'
            ]);

            Role::create([
                'name'=>'admin',
            ]);
            Role::create([
                'name'=>'user',
            ]);
            Role::create([
                'name'=>'moderator'
            ]);
           User::find(1)->assignRole('admin');
           User::find(2)->assignRole('moderator');

        }

    }
}
